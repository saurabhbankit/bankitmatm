package com.bankit.myapplication;

import android.app.Application;
import android.content.Context;

import androidx.annotation.WorkerThread;
import androidx.multidex.MultiDex;

import com.bankit.microatm.Volley.MySingleton;
import com.finopaytech.finosdk.FinoApplication;


public class MyApplication extends FinoApplication {

    public  MyApplication sInstance;

    Context context;
    public static final String TAG = "Chatse";
    private WorkerThread mWorkerThread;

    @Override
    public void onCreate() {
        super.onCreate();
        MySingleton.getInstance(this);
        sInstance = this;

    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }


}
