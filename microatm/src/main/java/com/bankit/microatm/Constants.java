package com.bankit.microatm;

public class Constants {

    public static final String RETURN_URL = "https://services.bankit.in:8443/WalletV1/wallet/matmload";// This URL is provided by Client to send ResponsecardLists.get(mPosition)
    public static final String SERVICE_MICRO_CW = "156";//MicroATM_CashWithdraw
    public static final String SERVICE_MICRO_BE = "157";//MicroATM_BalanceEnquiry
    public static final String FINOAUTHURL = "https://services.bankit.in:8443/WalletV1/wallet/matmauthenticate";
    public static final int VOLLEY_TIMEOUT_MS = 40000;
    public static final int VOLLEY_MAX_RETRIES = 0;
    public static final float VOLLEY_BACKOFF_MULT = 1.0F;
}
