package com.bankit.microatm;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.bankit.microatm.adapter.MicroAtmTabsAdapter;
import com.google.android.material.tabs.TabLayout;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MicroAtmTransactionsActivity extends AppCompatActivity {

    @BindView(R2.id.tabs)
    TabLayout tabs;
    @BindView(R2.id.viewpager)
    ViewPager viewPager;
    @BindView(R2.id.back)
    ImageView back;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_micro_atm_transactions);
        try {
            ButterKnife.bind(this);
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
            setupTabs();
        } catch (Exception e) {
            Log.e("error",e.getMessage());

        }

    }

    private void setupTabs() {
        try {
            MicroAtmTabsAdapter adapter = new MicroAtmTabsAdapter(getSupportFragmentManager());
            // Set the adapter onto the view pager
            viewPager.setAdapter(adapter);
            tabs.setupWithViewPager(viewPager);
       } catch (Throwable e) {

       }

    }








}
