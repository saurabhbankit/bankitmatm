package com.bankit.microatm;

import androidx.annotation.Keep;

import com.mobsandgeeks.saripaar.AnnotationRule;

/**
 * Created by mobileware on 12/4/17.
 */
@Keep
public class PhoneValidationRule extends AnnotationRule<CustomPhoneValidator, String> {

  protected PhoneValidationRule(CustomPhoneValidator haggle) {
    super(haggle);
  }

  @Override public boolean isValid(String phone) {

    boolean isValid = false;
    String[] array = { "6","7", "8", "9" };
    for (String beginning : array) {
      if (phone.trim().startsWith(beginning)) {
        isValid = true;
        break;
      }
    }
    return isValid;
  }
}