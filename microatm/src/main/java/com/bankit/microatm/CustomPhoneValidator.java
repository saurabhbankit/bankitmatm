package com.bankit.microatm;

import androidx.annotation.Keep;

import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by mobileware on 12/4/17.
 */

@Keep
@ValidateUsing(com.bankit.microatm.PhoneValidationRule.class) @Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD) public @interface CustomPhoneValidator {

  public int messageResId() default -1;                     // Mandatory attribute

  // public String message()     default ICsts.WRONG_PHONE_NUMBER;   // Mandatory attribute
  public String message() default "";   // Mandatory attribute

  public int sequence() default -1;                     // Mandatory attribute
}