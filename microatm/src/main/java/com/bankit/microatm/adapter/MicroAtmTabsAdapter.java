package com.bankit.microatm.adapter;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.bankit.microatm.fragment.MATMBEnquiry;
import com.bankit.microatm.fragment.MATMCWithdrawl;


public class MicroAtmTabsAdapter extends FragmentPagerAdapter {


    public MicroAtmTabsAdapter(FragmentManager fm) {
        super(fm);
    }

    // This determines the fragment for each tab
    @Override
    public Fragment getItem(int position) {
        if (position == 0) {


            return  new MATMCWithdrawl();
        } else if (position == 1) {


            return new MATMBEnquiry();
        } else {


            return new MATMCWithdrawl();
        }
    }

    // This determines the number of tabs
    @Override
    public int getCount() {
        return 2;
    }

    // This determines the title for each tab
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        switch (position) {
            case 0:
                return "Cash Withdraw";
            case 1:
                return "Balance Enquiry";
           /* case 2:
                return "Fund Transfer";*/
            default:
                return null;
        }
    }
}
