package com.bankit.microatm;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import androidx.annotation.Nullable;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.gun0912.tedpermission.TedPermission;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Random;
import static android.text.TextUtils.isEmpty;


public class Utils {

    static long toastTime;
    private static ProgressDialog mProgressDialog;

    private Utils() {
    }

    public static void hideKeyboard(Context ctx) {
        try {
            InputMethodManager inputManager =
                    (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);

            // check if no view has focus:
            View v = ((Activity) ctx).getCurrentFocus();
            if (v == null) return;
            if (inputManager != null) {
                inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
            }

        } catch (Throwable e) {

        }

    }

    public static void showkeyBoard(Context ctx) {
        try {
            View view = ((Activity) ctx).getCurrentFocus();
            InputMethodManager methodManager = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            assert methodManager != null && view != null;
            methodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        } catch (Throwable e) {

        }

    }

    public static BigDecimal round(Float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd;
    }

    public static BigDecimal round(String d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(d);
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd;
    }

    public static boolean validatePhoneNumber(String phoneNo) {
        return phoneNo.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\-]\\s*)?|[0]?)?[6789]\\d{9}$");
    }


    public static void showSnack(Context context, boolean isConnected) {
        String message;
        if (!isConnected) {
            message = "Please check internet connection";

            if (!((Activity) context).isFinishing()) {
                new AlertDialog.Builder(context).setMessage(message)
                        .setCancelable(false)
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {//
                            }
                        })
                        .show();
            }
        }
    }

    public static void requestFocus(View view, Activity activity) {
        if (view.requestFocus()) {
            activity.getWindow()
                    .setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    public static int getNumberDigits(String inString) {
        if (isEmpty(inString)) {
            return 0;
        }
        int numDigits = 0;
        int length = inString.length();
        for (int i = 0; i < length; i++) {
            if (Character.isDigit(inString.charAt(i))) {
                numDigits++;
            }
        }
        return numDigits;
    }

    public static int getSpecialCharacterCount(String s) {
        int theCount = 0;
        char c;
        int cint;
        for (int n = 0; n < s.length(); n++) {
            c = s.charAt(n);
            cint = (int) c;
            if (cint < 48 || (cint > 57 && cint < 65) || (cint > 90 && cint < 97) || cint > 122) {
                theCount = theCount + 1;
            }
        }
        return theCount;
    }

    public static int getCaps(String inString) {
        if (isEmpty(inString)) {
            return 0;
        }
        int caps = 0;
        int length = inString.length();
        for (int i = 0; i < length; i++) {
            if (Character.isUpperCase(inString.charAt(i))) {
                caps++;
            }
        }
        return caps;
    }

    public static int getLowerCase(String inString) {
        if (isEmpty(inString)) {
            return 0;
        }
        int lowercase = 0;
        int length = inString.length();
        for (int i = 0; i < length; i++) {
            if (Character.isLowerCase(inString.charAt(i))) {
                lowercase++;
            }
        }
        return lowercase;
    }

    public static String getJsonValueFromJsonObject(JSONObject jsonObject, String strKey) {
        String strValue = "";
        try {
            if (jsonObject.has(strKey)) {
                strValue = jsonObject.getString(strKey);
            } else {
                strValue = "";
            }
        } catch (JSONException e) {

        }
        return strValue;
    }

    public static String checkConnectionError(VolleyError volleyError) {

        String message = null;
        if (volleyError instanceof ServerError) {
            message = "The server could not be found. Please try again after some time!!";
        } else if (volleyError instanceof AuthFailureError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof ParseError) {
            message = "Parsing error! Please try again after some time!!";
        } else if (volleyError instanceof NoConnectionError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else if (volleyError instanceof TimeoutError) {
            message = "Connection TimeOut! Please check your internet connection.";
        } else if (volleyError instanceof NetworkError) {
            message = "Cannot connect to Internet...Please check your connection!";
        } else {
            try {
                if (volleyError.networkResponse != null) {
                    if (volleyError.networkResponse.data != null) {
                        String responseBody = new String(volleyError.networkResponse.data, "utf-8");
                        JSONObject exception = new JSONObject(responseBody);
                        message = exception.getString("message");
                    }
                }


            } catch (JSONException e) {

//Handle a malformed json response
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        return message;
    }
    public static String createMultipleTransactionID() {
        String AgentTranID = "";
        try {


            SimpleDateFormat sdf = new SimpleDateFormat("yyMMddHHmmssSS");
            Date date = new Date();
            String tranID = sdf.format(date);
            int n = 6;
            Random randGen = new Random();
            int startNum = (int) Math.pow(10, n - 1);
            int range = (int) (Math.pow(10, n) - startNum);
            int randomNum = randGen.nextInt(range) + startNum;
            String ran = String.valueOf(randomNum);
            AgentTranID = tranID + ran;
            Log.e("AgentTranID",AgentTranID);
        } catch (Throwable e) {

        }
        return AgentTranID;
    }



    public static String formatPIDData(String biometricData) {
        try {
            Log.d("old_pid", "original biometricData" + biometricData);
            JSONObject
                    jsonObject = XML.toJSONObject(biometricData);
            JSONObject PidData = jsonObject.optJSONObject("PidData");
            if (PidData.has("DeviceInfo")) {
                JSONObject deviceInfo = PidData.optJSONObject("DeviceInfo");
                if (!deviceInfo.has("additional_info")) {

                    JSONObject additional_info = new JSONObject();
                    JSONArray paramArray = new JSONArray();
                    JSONObject param = new JSONObject();
                    param.put("name", "srno");
                    param.put("value", deviceInfo.optString("dc"));
                    JSONObject param1 = new JSONObject();
                    param1.put("name", "serial_number");
                    param1.put("value", deviceInfo.optString("dc"));
                    paramArray.put(0, param);
                    paramArray.put(1, param1);
                    JSONObject finalParam = new JSONObject();
                    finalParam.put("Param", paramArray);
                    additional_info.put("additional_info", finalParam);
                    deviceInfo.put("additional_info", finalParam);
                    PidData.put("DeviceInfo", deviceInfo);
                    jsonObject.put("PidData", PidData);
                    biometricData = XML.toString(jsonObject);
                }
            }
            Log.d("new biometric_pid", "biometricData" + biometricData);
        } catch (JSONException e) {
            return biometricData;
        }
        return biometricData;
    }

    @SuppressLint({"MissingPermission", "HardwareIds"})
    public static String getDeviceImei(Context ctx) {
        String imei_number = "";
        try {

            if (TedPermission.isGranted(ctx, new String[]{"android.permission.READ_PHONE_STATE"})) {
                TelephonyManager telephonyManager = (TelephonyManager) ctx.getSystemService(Context.TELEPHONY_SERVICE);

                if (telephonyManager != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        imei_number = telephonyManager.getImei();
                    }
                    else {

                        imei_number = telephonyManager.getDeviceId();
                    }
                }
            }

        }catch (Exception e){
             imei_number = Settings.Secure.getString(ctx.getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        return imei_number;
    }

    private static String convertToHex(byte[] data) {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9))
                    buf.append((char) ('0' + halfbyte));
                else
                    buf.append((char) ('a' + (halfbyte - 10)));
                halfbyte = data[i] & 0x0F;
            } while (two_halfs++ < 1);
        }
        return buf.toString();
    }


    public static String SHA1(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {

        MessageDigest md;
        md = MessageDigest.getInstance("SHA-1");
        byte[] sha1hash = new byte[40];
        md.update(text.getBytes("iso-8859-1"), 0, text.length());
        sha1hash = md.digest();
        return convertToHex(sha1hash);
    }

    public static String maskNo(String number) {

        for (int i = 0; i < number.length(); i++) {
            int n = number.length();
            if (n % 4 == 0) {
                number = number + " ";
            }
        }


        return number;
    }


    public static String getTrnId() {


        String tranid = "";

        try {

            int n = 9;

            Random randGen = new Random();

            int startNum = (int) Math.pow(10, n - 1);

            int range = (int) (Math.pow(10, n) - startNum);

            int randomNum = randGen.nextInt(range) + startNum;

            String str = String.valueOf(randomNum);

            int j = 8;

            Random randGn = new Random();

            int startNm = (int) Math.pow(10, j - 1);

            int range1 = (int) (Math.pow(10, j) - startNm);

            int randomNum2 = randGn.nextInt(range1) + startNm;

            String str1 = String.valueOf(randomNum2);

            tranid = str + "V" + str1;


        } catch (Throwable e) {


        }

        return tranid;

    }






    public static String handleResponse(String tag, NetworkResponse response) {
        StringBuilder output = new StringBuilder(); // note: better to use StringBuilder
        try {
            try {
                final InputStreamReader reader = new InputStreamReader(new ByteArrayInputStream(response.data));
                final BufferedReader in = new BufferedReader(reader);
                String read;
                while ((read = in.readLine()) != null) {
                    output = output.append(read);
                }
                reader.close();
                in.close();
                String str = output.toString();
                Log.e(tag, str);
                return output.toString();
            } catch (Throwable e) {
            }

        } catch (Throwable e) {
        }
        return "";
    }






    @Nullable
    public static String getNetworkInterfaceIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface networkInterface = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = networkInterface.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress() && inetAddress instanceof Inet4Address) {
                        String host = inetAddress.getHostAddress();
                        if (!TextUtils.isEmpty(host)) {
                            return host;
                        }
                    }
                }

            }
        } catch (Throwable ex) {

        }
        return null;
    }

    /**
     * Hide progress dialog
     */
    public static void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            try {
                mProgressDialog.dismiss();
                mProgressDialog = null;

            } catch (Exception e) {
                // TODO: handle exception
                e.printStackTrace();
            }

        } else {
            // showErrorLog("", "mProgressDialog is null");

        }
    }

    /**
     * Showing progress dialog
     *
     * @param msg message to show progress dialog
     */
    public static void showProgressDialog(final Activity mActivity, final String msg, final boolean isCancelable) {
        try {
            if (mActivity != null && mProgressDialog != null
                    && mProgressDialog.isShowing()) {
                try {
                    mProgressDialog.dismiss();
                    mProgressDialog = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
            mProgressDialog = null;
            if (mProgressDialog == null && mActivity != null) {
                mActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mProgressDialog = new ProgressDialog(mActivity, R.style.MyAlertDialogStyle);
                        mProgressDialog.setMessage(msg);
                        mProgressDialog.setCancelable(isCancelable);
                    }
                });

            }
            if (mActivity != null && mProgressDialog != null
                    && !mProgressDialog.isShowing()) {
                mProgressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }




    public static void parseVolleyError(VolleyError error, Context context) {
        try {

            if(error.networkResponse.statusCode==404){
                showAlert("Server is not available, Please try later!",context);
            }else{

                String responseBody = new String(error.networkResponse.data, "utf-8");
                JSONObject data = new JSONObject(responseBody);
                String message = data.optString("message");

                showAlert(message,context);
            }


        } catch (Exception e) {
        }
    }

    private static void showAlert(String msg,Context context) {

        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
//        alertDialog.setTitle("Alert");
        alertDialog.setMessage(msg);
        alertDialog.setCancelable(false);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
