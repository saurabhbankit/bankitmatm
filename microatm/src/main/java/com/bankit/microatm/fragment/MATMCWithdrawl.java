package com.bankit.microatm.fragment;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;


import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.bankit.microatm.BuildConfig;
import com.bankit.microatm.Constants;
import com.bankit.microatm.CustomPhoneValidator;
import com.bankit.microatm.GetLocation;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import it.sephiroth.android.library.tooltip.Tooltip;

import com.bankit.microatm.R;
import com.bankit.microatm.R2;
import com.bankit.microatm.Utils;
import com.bankit.microatm.Volley.MySingleton;

import com.finopaytech.finosdk.activity.MainTransactionActivity;
import com.finopaytech.finosdk.encryption.AES_BC;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.Max;
import com.mobsandgeeks.saripaar.annotation.Min;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;


public class MATMCWithdrawl extends Fragment implements Validator.ValidationListener {

    View v;

    Validator validator;

    @BindView(R2.id.tvTransactionID)
    TextView tvTransactionID;

    @BindView(R2.id.tvAmount)
    TextView tvAmount;

    @BindView(R2.id.tvStaus)
    TextView tvStaus;

    @BindView(R2.id.tvCashWishdrawlStatus)
    TextView tvCashWishdrawlStatus;

    @BindView(R2.id.cardViewDetails)
    CardView cardViewDetails;


    @BindView(R2.id.lvTop)
    LinearLayout lvTop;

    @NotEmpty(message = "Please enter mobile no")
    @CustomPhoneValidator(messageResId = R2.string.wrongPhone)
    @Length(max = 10, min = 10, message = "Please enter 10 digits mobile number")
    @BindView(R2.id.etMobileNumber)
    EditText etMobileNumber;


    @NotEmpty(message = "Please enter Amount")
    @Min(100)
    @Max(10000)
    @Length(max = 5, min = 3, message = "Enter Amount between 100 to 10000")
    @BindView(R2.id.etAmount)
    EditText etAmount;


    @BindView(R2.id.imageViewIcon)
    ImageView imageViewIcon;

    @BindView(R2.id.text_mobile_count)
    TextView text_mobile_count;
    // ProgressBarAsync progressBarAsync;

    private String bodyKey = "";

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.micro_atm_cash_widral_fragment, container, false);
        ButterKnife.bind(this, v);
        com.mobsandgeeks.saripaar.Validator.registerAnnotation(
                CustomPhoneValidator.class); // adds the custom Phone input field validation
        bottomToolTipDialogBox(etMobileNumber, "Please enter mobile no");
        etMobileNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View clickedView) {

                clickedView.clearFocus();
                clickedView.requestFocus();

            }
        });

        etMobileNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFous) {
                if (hasFous) {
                    bottomToolTipDialogBox(etMobileNumber, "Please enter mobile no");
                }
            }
        });


        etAmount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View clickedView) {

                clickedView.clearFocus();
                clickedView.requestFocus();

            }
        });

        etAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFous) {
                if (hasFous) {
                    bottomToolTipDialogBox(etAmount, "Enter Amount between 100 to 10000");
                }
            }
        });


        showKeyboard(etMobileNumber);
        etMobileNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                try {
                    text_mobile_count.setText("Digit-" + etMobileNumber.getText().toString().length());
                    if (s.length() == 10) {
                        etAmount.requestFocus();
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void afterTextChanged(Editable s) {


            }
        });


        return v;
    }


    public String getEncryptedRequest(String Version, String ClientRefID, String bodyKey) {
        String strRequestData = "";
        JSONObject jsonRequestDataObj = new JSONObject(); // inner object request
        try {

            jsonRequestDataObj.put("MerchantId", "8512001213");
            jsonRequestDataObj.put("SERVICEID", Constants.SERVICE_MICRO_CW);
            jsonRequestDataObj.put("RETURNURL", Constants.RETURN_URL);
            jsonRequestDataObj.put("Version", Version);


            jsonRequestDataObj.put("Amount", etAmount.getText().toString());
            jsonRequestDataObj.put("ClientRefID", ClientRefID);
            tvTransactionID.setText(ClientRefID);
            this.bodyKey = bodyKey;
            strRequestData = com.finopaytech.finosdk.helpers.Utils.replaceNewLine(AES_BC.getInstance().encryptEncode(jsonRequestDataObj.toString(), bodyKey));
        } catch (Exception e) {
        }
        return strRequestData;
    }


    private static String getEncryptedHeader(String AuthKey, String ClientId, String headerKey) {
        String strHeader = "";
        JSONObject header = new JSONObject();
        try {
            header.put("AuthKey", AuthKey);
            header.put("ClientId", ClientId);
            strHeader = com.finopaytech.finosdk.helpers.Utils.replaceNewLine(AES_BC.getInstance().encryptEncode(header.toString(), headerKey));
        } catch (JSONException ex) {

        }
        return strHeader;
    }

    public void bottomToolTipDialogBox(View view, String strMsg) {
        try {
            Tooltip.make(getActivity(),
                    new Tooltip.Builder(101).withStyleId(R.style.ToolTipLayoutHoianStyle)
                            .anchor(view, Tooltip.Gravity.BOTTOM)
                            .closePolicy(
                                    new Tooltip.ClosePolicy().insidePolicy(true, false).outsidePolicy(true, false),
                                    2000)
                            //.activateDelay(900)
                            //.showDelay(400)
                            .text(strMsg)
                            .maxWidth(600)
                            .withArrow(true)
                            .withOverlay(true)
                            .build()).show();
        } catch (Throwable e) {

        }
    }

    private void showKeyboard(View view) {
        try {
            if (getActivity() != null) {
                InputMethodManager imm =
                        (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                if (imm != null)
                    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
                view.requestFocus();
                if (view instanceof EditText) {
                    ((EditText) view).setSelection(((EditText) view).getText().length());
                }
            }
        } catch (Throwable e) {
        }

    }

    private void clearData() {

        try {
            etMobileNumber.getText().clear();
            etAmount.getText().clear();
        } catch (Throwable e) {

        }
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        try {
            validator = new Validator(this);
            validator.setValidationListener(this);
        } catch (Throwable e) {
        }
    }

    @Override
    public void onValidationSucceeded() {

        try {
            Utils.hideKeyboard(getActivity());
            getFinoAuthCredentials();
        } catch (Throwable e) {

        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        try {
            for (ValidationError error : errors) {
                View view = error.getView();
                String message = error.getCollatedErrorMessage(getActivity());

                // Display error messages ;)
                if (view instanceof EditText) {
                    ((EditText) view).setError(message);
                } else {
                    Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
                }
            }
        } catch (Throwable e) {

        }

    }

    @OnClick(R2.id.tvSubmit)
    public void submit(View view) {

        try {
            validator.validate();
            Utils.hideKeyboard(getActivity());
        } catch (Throwable e) {

        }
    }

    @OnClick(R2.id.tcCancel)
    public void cancel(View view) {

        try {
            if (getActivity() != null)
                getActivity().finish();
        } catch (Throwable e) {

        }

    }

    private void getFinoAuthCredentials() {
        Utils.showProgressDialog(getActivity(), "Please wait", false);
        String location;
        GetLocation getLocation = new GetLocation(getActivity());

        if (getLocation.getLocation() == null) {
            location = "Lat :" + getLocation.getLatitude() + "/Long :" + getLocation.getLongitude() + "/Address :" + " " + "/versionName:" + BuildConfig.VERSION_NAME + "/versionCode:" + BuildConfig.VERSION_CODE;

        } else {
            location = "Lat :" + getLocation.getLatitude() + "/Long :" + getLocation.getLongitude() + "/Address :" + getLocation.getAddress(getLocation.getLatitude(), getLocation.getLongitude()) + "/versionName:" + BuildConfig.VERSION_NAME + "/versionCode:" + BuildConfig.VERSION_CODE;
        }

        String url = Constants.FINOAUTHURL;
//        final Realm realm = Realm.getDefaultInstance();
//        RealmQuery<User> userRealmQuery = realm.where(User.class);
//        final User user = userRealmQuery.findFirst();

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobileNo", etMobileNumber.getText().toString().trim());
            jsonObject.put("agentId", "AG2182");
            jsonObject.put("amount", etAmount.getText().toString().trim());
            jsonObject.put("serviceId", Constants.SERVICE_MICRO_CW);
            jsonObject.put("extraInfo", "Location:" + location + " DeviceIMEI:" + Utils.getDeviceImei(getActivity()) + " IPAddress:" +/*Utils.getIpAddressDevice()*/"");

        } catch (JSONException e) {

        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, jsonObject, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject jsonObject) {


                try {

                    Utils.hideProgressDialog();
                    if (jsonObject.getString("statusCode").equals("0")) {


                        try {
                            Intent intent = new Intent(getActivity(), MainTransactionActivity.class);
                            intent.putExtra("RequestData", getEncryptedRequest(jsonObject.optString("version"), jsonObject.optString("txnId"), jsonObject.optString("bodyKey")));
                            intent.putExtra("HeaderData", getEncryptedHeader(jsonObject.optString("authKey"), jsonObject.optString("clientId"), jsonObject.optString("header")));
                            intent.putExtra("ReturnTime", 5);// Application return time in second
                            startActivityForResult(intent, 1);
                        } catch (Throwable e) {
                        }


                    } else
                        new AlertDialog.Builder(getActivity()).setMessage("Unable To Fetch Info, Please Try later")
                                .setCancelable(false)
                                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {

                                        try {
                                            etAmount.setText("");
                                            etAmount.setText(null);
                                            etMobileNumber.setText("");
                                            etMobileNumber.setText(null);
                                        } catch (Throwable e) {

                                        }

                                    }
                                })
                                .show();
                } catch (JSONException e) {
                    Utils.hideProgressDialog();

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Utils.hideProgressDialog();
                String message = Utils.checkConnectionError(volleyError);
                Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
            }
        });
        jsonObjectRequest.setRetryPolicy(
                new DefaultRetryPolicy(Constants.VOLLEY_TIMEOUT_MS, Constants.VOLLEY_MAX_RETRIES,
                        Constants.VOLLEY_BACKOFF_MULT));
        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
//        realm.close();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        clearData();
    }
}
